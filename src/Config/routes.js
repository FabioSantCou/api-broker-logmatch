const express = require('express')
module.exports = function(server)
{

    // const api = express.Router()
    // server.use('/api', api)

    const openApi = express.Router()
    server.use('/oapi', openApi)

    const ficha =require('../Ficha/fichaService')
    ficha.register(openApi,'/ficha')

    const AuthService = require('../User/authService')
    openApi.post('/login', AuthService.login)
    openApi.post('/signup', AuthService.signup)
    openApi.post('/validateToken', AuthService.validateToken)

    const protectedApi = express.Router()
    server.use('/api', protectedApi)


}

