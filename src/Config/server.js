const port = 3001
const bodyParser = require('body-parser')
const express = require('express')
const server = express()
const {parametros, topicosMQTT} = require("../constants")
var mqtt = require('mqtt')
var request = require('request')
const allowCors = require('./cors')
const queryParser = require('express-query-int')
var fs = require('fs');
var request = require('request')
var moment = require('moment')


let arrayController=[]
let arrayStatus=[]
let agora=moment()

// var pathCertificate = process.env.PATH_CERTIFICATE
// var serverFile = fs.readFileSync(pathCertificate)

server.use(bodyParser.urlencoded({extended:true})) // serve para fazer um parse de um formulário
server.use(bodyParser.json()) // para fazer o parse de um objeto json
server.use(queryParser())
server.use(allowCors)

const openApi = express.Router()
server.use('/apicontroller', openApi)


const timeOutReq = 60 //valor de 60 = 30 segundos


//posts da api ----------------------------------------------------

openApi.post('/reference/voltage', (req,res,next)=>{
  const faseMin = req.body.faseMin || ''
  const faseMax = req.body.faseMax || ''
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''

  let jsonMqtt = {
    id:controllerId,
    faseMin:faseMin,
    faseMax:faseMax,
    reqId:reqId
  }

  client.publish(topicosMQTT.topicoReferenciaFase,JSON.stringify(jsonMqtt))
  

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();

  // setTimeout(function() {

  //   if(arrayStatus.length!=0){
  //     res.json({status:arrayStatus})   
  //   }else{
  //     res.sendStatus(600)
  //   }
  // }, timeReq);

  // arrayStatus=[]
})

openApi.post('/reference/temperature', (req,res,next)=>{
  const tempMin = req.body.tempMin || ''
  const tempMax = req.body.tempMax || ''
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''


let jsonMqtt = {
  id:controllerId,
  tempMin:tempMin,
  tempMax:tempMax,
  reqId:reqId
}
  

  client.publish(topicosMQTT.topicoReferenciaTemperatura,JSON.stringify(jsonMqtt))
  

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/getplan', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''

  let jsonMqtt = {
    id:controllerId,
    reqId:reqId
  }

  client.publish(topicosMQTT.topicoSubscribePlanoSemaforico,JSON.stringify(jsonMqtt))

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            if(item.statusCode === 100)
            {
              let jsonPlan = {
                Ciclo:item.ciclo,
                Data:item.data,
                Date:item.date
              }
              flagAchou=true;
              res.json(jsonPlan)
              console.log(item);
              console.log(arrayStatus);
              arrayStatus.splice(indice,1)
              console.log(arrayStatus);
              next();
            }else{
              flagAchou=true;
              res.sendStatus(800)
              console.log(item);
              console.log(arrayStatus);
              arrayStatus.splice(indice,1)
              console.log(arrayStatus);
              next();
            }
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/getcurrents', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || '' 

let jsonMqtt = {
  id:controllerId,
  reqId:reqId
}

client.publish(topicosMQTT.topicoSubscribeChecaCorrentes,JSON.stringify(jsonMqtt))


  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            let jsonPlan = {
              Data:item.data,
            }
            res.json(jsonPlan)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/resetcontroller', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''
  const estado = req.body.estado || ''
 
  let jsonMqtt = {
    id:controllerId,
    estado:estado,
    reqId:reqId
  }

  console.log(jsonMqtt)
  client.publish(topicosMQTT.topicoSubscribeComandaReleA,JSON.stringify(jsonMqtt))
  
  flagAchou=false
  contTempo=0;

  // setTimeout(function() {

  //   if(arrayStatus.length!=0){
  //     res.json({responseIds:arrayStatus})   
  //   }else{
  //     res.sendStatus(600)
  //   }
  // }, timeReq);

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();

  
  
  // emitter.on('idReq',(message)=>{
  //   if(message == reqId)
  //     res.sendStatus(200)
  //     //emitter.removeListener(idReq)
  //     //return
  // })
  
  //res.sendStatus(200)

  //arrayStatus=[]
})

openApi.post('/command/relay', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''
  const estado = req.body.estado || ''

  let jsonMqtt = {
    id:controllerId,
    estado:estado,
    reqId:reqId
  }

  client.publish(topicosMQTT.topicoSubscribeComandaReleB,JSON.stringify(jsonMqtt))

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();

  // setTimeout(function() {

  //   if(arrayStatus.length!=0){
  //     res.json({responseIds:arrayStatus})   
  //   }else{
  //     res.sendStatus(600)
  //   }
  // }, timeReq);

  // arrayStatus=[]
})

openApi.post('/reference/planA', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''
  const plan = req.body.plan || ''

  let strPlan = ""+controllerId+plan+reqId

  console.log(strPlan)

  client.publish(topicosMQTT.topicoSubscribeRecebePlanoA,strPlan)
  

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/reference/planB', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''
  const plan = req.body.plan || ''


  let strPlan = ""+controllerId+plan+reqId

  client.publish(topicosMQTT.topicoSubscribeRecebePlanoB,strPlan)

  

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/reference/planC', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''
  const tmin = req.body.tmin || ''
  const tmax = req.body.tmax || ''
  const tempMin = req.body.tempMin || ''
  const tempMax = req.body.tempMax || ''
  const od1 = req.body.od1 || ''
  const od2 = req.body.od2 || ''
  const id1 = req.body.id1 || ''
  const id2 = req.body.id2 || ''
  const ciclo = req.body.ciclo || ''
  const hb = req.body.hb || ''

  let jsonMqtt = {
    id:controllerId,
    reqId:reqId,
    tmin:tmin,
    tmax:tmax,
    tempMin:tempMin,
    tempMax:tempMax,
    od1:od1, 
    od2:od2,
    id1:id1,
    id2:id2,
    ciclo:ciclo,
    hb:hb
  }
  

  console.log(JSON.stringify(jsonMqtt))

  client.publish(topicosMQTT.topicoSubscribeRecebePlanoC,JSON.stringify(jsonMqtt))


  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/reference/input', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''
  const ref = req.body.ref || ''

  let jsonMqtt = {
    id:controllerId,
    ref:ref,
    reqId:reqId
  }

  console.log(jsonMqtt)

  client.publish(topicosMQTT.topicoSubscribeEntradaReferencia,JSON.stringify(jsonMqtt))

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.sendStatus(200)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/disablecontroller', (req,res,next)=>{
  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''

  let jsonMqtt = {
    id:controllerId,
    reqId:reqId
  }

  console.log(jsonMqtt)

  console.log(arrayController)

  arrayController.forEach(function (item, indice) 
  {
    if(item.controllerId == controllerId)
    {
      console.log(indice)
      arrayController.splice(indice,1)
      console.log(arrayController);
    }
  })

  res.sendStatus(200)

  // client.publish(topicosMQTT.topicoSubscribeDesabilitaControladora,JSON.stringify(jsonMqtt))

  // setTimeout(function() {

  //   if(arrayStatus.length!=0){
  //     res.json({responseIds:arrayStatus})   
  //   }else{
  //     res.sendStatus(600)
  //   }
  // }, timeReq);

  // arrayStatus=[]
})

openApi.post('/getlocation',(req,res,next)=>{

  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''


  let jsonMqtt = {
    id:controllerId,
    reqId:reqId
  }

  client.publish(topicosMQTT.topicoSubscribeLocalizacao,JSON.stringify(jsonMqtt))


  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            let jsonPlan = {
              Latitude:item.latitude/1000000,
              Longitude:item.longitude/1000000
            }
            res.json(jsonPlan)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/getfirmware',(req,res,next)=>{

  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''

  let jsonMqtt = {
    id:controllerId,
    reqId:reqId
  }

  client.publish(topicosMQTT.topicoSubscribeFirmware,JSON.stringify(jsonMqtt))


  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            let jsonPlan = {
              Version:item.version,
              Compilation:item.compilation
            }
            res.json(jsonPlan)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/newid',(req,res,next)=>{

  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''
  const newId = req.body.newId || ''

  let jsonMqtt = {
    id:controllerId,
    reqId:reqId,
    newId:newId
  }

  client.publish(topicosMQTT.topicoSubscribeTrocarId,JSON.stringify(jsonMqtt))

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        res.sendStatus(600)
        return
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            let jsonResp = {
              Status:"OK",
            }
            res.json(jsonResp)
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})

openApi.post('/health',(req,res,next)=>{

  const controllerId = req.body.controllerId || ''
  const reqId = req.body.reqId || ''

  let jsonMqtt = {
    id:controllerId,
    reqId:reqId,
  }

  client.publish(topicosMQTT.topicoSubscribeFirmware,JSON.stringify(jsonMqtt))

  flagAchou=false
  contTempo=0

  var checkResponse = () =>{
    setTimeout(function() {
      if(contTempo == timeOutReq)
      {
        return res.status(501).send(`Houve uma falha de comunicação com o controlador ${controllerId}`)
      }else{
        console.log(arrayStatus);
        arrayStatus.forEach(function (item, indice) {
          if(item.reqId === reqId)
          {
            flagAchou=true;
            res.send("OK")
            console.log(item);
            console.log(arrayStatus);
            arrayStatus.splice(indice,1)
            console.log(arrayStatus);
            next();
          }
        });
        if(!flagAchou){
          checkResponse();
          contTempo = contTempo+1
          //console.log(contTempo)
        }
      }
    }, 500);
  }

  checkResponse();
})
//----------------------------------------------------

let securityOptions = {
  username: parametros.username,
  password: parametros.password,
  // rejectUnauthorized: true,
  // keepAlive: 1000,
  // connectTimeout: 5000,
  // ca: serverFile
}

function makeGetError(urlGet) {
  request.get(urlGet,"",function(err,httpResponse,body){
    if (err) {
        return console.error('upload failed:', err);
    }
    console.log(httpResponse.statusCode)//, body);
  })
}

var client = mqtt.connect(parametros.broker,securityOptions)
 
client.on('connect', function () {
  client.subscribe(topicosMQTT.topicoErroTensaoFase, function (err) {
    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoErroTensaoFase)
    }else{
      console.log("ERRO "+err);
    }
  })

  client.subscribe(topicosMQTT.topicoErroPortasDigitais, function (err) {
    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoErroPortasDigitais)
    }
  })

  client.subscribe(topicosMQTT.topicoErroCorrenteFase, function (err) {
    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoErroCorrenteFase)
    }
  })

  client.subscribe(topicosMQTT.topicoErroGPS, function (err) {
    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoErroGPS)
    }
  })

  client.subscribe(topicosMQTT.topicoErroFlashAmarelo, function (err) {
    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoErroFlashAmarelo)
    }
  })

  client.subscribe(topicosMQTT.topicoErroTemperatura, function (err) {
    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoErroTemperatura)
    }
  })

  client.subscribe(topicosMQTT.topicoEnviaPlanoSemaforico, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoEnviaPlanoSemaforico)
    }
  })

  client.subscribe(topicosMQTT.topicoEnviaHeartBeat, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoEnviaHeartBeat)
    }
  })

  client.subscribe(topicosMQTT.topicoErroCicloMax, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoErroCicloMax)
    }
  })

  client.subscribe(topicosMQTT.topicoEnviaCorrenteSemaforos, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoEnviaCorrenteSemaforos)
    }
  })

  client.subscribe(topicosMQTT.topicoDesabilitaControladora, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoDesabilitaControladora)
    }
  })

  client.subscribe(topicosMQTT.topicoControladoraSemErro, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoControladoraSemErro)
    }
  })

  client.subscribe(topicosMQTT.topicoCallback, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoCallback)
    }
  })

  client.subscribe(topicosMQTT.topicoEnviaErroBotoeira, function (err) {

    if (!err) {
      console.log('Inscrito no Tópico - '+ topicosMQTT.topicoEnviaErroBotoeira)
    }
  })
})

client.on('error', (error) => {
  console.log('Error:', error);
});
 
client.on('message', function (topic, message) {
  // message is Buffer
  let obj

  if(topic == topicosMQTT.topicoErroTensaoFase)
  {
    let controllerItemId,description,codigoErro
    obj = JSON.parse(message.toString());
    controllerItemId = obj.id
    description = "Leitura: " +obj.leitura //+ " / Temperatura: " + obj.temp
    codigoErro = parametros.codigoErroTensaoFase;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp 

    console.log(urlSet)
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoErroPortasDigitais)
  {
    let controllerItemId,description,codigoErro
    obj = JSON.parse(message.toString());
    controllerItemId = obj.id
    description = "Porta 01: " + obj.portaID1 + " / Porta 02: " + obj.portaID2// + " / Temperatura: " + obj.temp
    codigoErro = parametros.codigoErroPortasDigitais;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp 
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoErroCorrenteFase)
  {
    let controllerItemId,description,codigoErro,fase, entrada
    obj = JSON.parse(message.toString());
    fase = parseInt((obj.fase.valueOf()-1)/3) + 1

    if(obj.fase.valueOf()%3 == 1)
    {
      entrada = "Verde"
    }else if(obj.fase.valueOf()%3 == 2)
    {
      entrada = "Amarela"
    }else if(obj.fase.valueOf()%3 == 0)
    {
      entrada = "Vermelha"
    }

    controllerItemId = obj.id
    description = "Defeito na entrada " + entrada + " da Fase " + fase + " / Corrente : " + obj.leitura + "mA" // / Temperatura : " + obj.temp
    console.log(description)
    codigoErro = parametros.codigoErroCorrenteFase;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp 
    console.log(urlSet)
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoErroGPS)
  {
    let controllerItemId,description,codigoErro
    obj = JSON.parse(message.toString());
    controllerItemId = obj.id
    description = ""//" Temperatura: " + obj.temp
    codigoErro = parametros.codigoErroGPS;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp     
    console.log(urlSet)
    makeGetError(urlSet)
  }
  
  else if(topic == topicosMQTT.topicoErroFlashAmarelo)
  {
    let controllerItemId,description,codigoErro
    obj = JSON.parse(message.toString());
    controllerItemId = obj.id
    description = ""//" Temperatura = "+obj.temp
    if(obj.fase != 0)
    {
      description = description + " / Fase " + obj.fase + " com defeito."
    }
    codigoErro = parametros.codigoErroFlashAmarelo;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoErroTemperatura)
  {
    let controllerItemId,description,codigoErro
    obj = JSON.parse(message.toString());
    controllerItemId = obj.id
    description = ""//"Temperatura: " +obj.temperatura
    codigoErro = parametros.codigoErroTemperatura;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temperatura 
    console.log(urlSet)
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoEnviaPlanoSemaforico)
  {
    const obj = JSON.parse(message.toString());

    let tempo = obj.tempo.split("/")

    let tempoFinal = moment(tempo[1], "HH:mm:ss").add(obj.gps,'seconds').format("YYYY-MM-DD/HH:mm:ss")


    let jsonPost = {
      controllerItemId:obj.id,
      ciclo:obj.ciclo,
      data:obj.leitura,
      date:tempoFinal
    }

    const options = {
      uri: 'https://webcentralsetrans.azurewebsites.net/api/lives/?password=webcentralsetran2021',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(jsonPost)
    }

    request.post(options,function(err,httpResponse,body){
          if (err) {
              return console.error('upload failed:', err);
          }
          console.log(httpResponse.statusCode)
      })
  }

  else if(topic == topicosMQTT.topicoEnviaHeartBeat)
  {
  
    let controllerItemId,description,codigoErro
    let flagAchou=false;

    obj = JSON.parse(message.toString());

    let controller = {
      controllerId:obj.id,
      controllerTime:(moment())
    }

    arrayController.forEach(function (item, indice) {
      if(item.controllerId === obj.id)
      {
        flagAchou=true;
        arrayController[indice].controllerTime = moment()
      }
    });

    if(!flagAchou)
    {
      arrayController.push(controller)
      flagAchou=false;
    }

    console.log(arrayController)

    arrayController.forEach(function (item, indice) {
      if(item.controllerId == obj.id)
      {
       console.log("Array = " + item.controllerId + " - " + indice)
      }
    });

    controllerItemId = obj.id
    description = ""
    //console.log(message.toString() + topic)
    codigoErro = parametros.codigoHeartBeat;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description
    console.log(urlSet)
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoErroCicloMax) //temperatura
  {
    let controllerItemId,description,codigoErro
    obj = JSON.parse(message.toString());
    controllerItemId = obj.id
    // description = "Defeito na entrada " + entrada + " da Fase " + fase + " / Corrente : " + obj.leitura + "mA" // / Temperatura : " + obj.temp

    // description = "Verde da Fase 1 com defeito"/// Temperatura: " + obj.temp
    description = "Defeito na entrada Verde da Fase 1 / Corrente : 0 mA"/// Temperatura: " + obj.temp
    codigoErro = parametros.codigoErroCicloMax;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp
    console.log(urlSet)
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoEnviaErroBotoeira) 
  {
    let controllerItemId,description,codigoErro,fase
    obj = JSON.parse(message.toString());
    controllerItemId = obj.id
    fase=obj.fase
    // description = "Erro em fase de botoeira ou laço - Fase "+fase/// Temperatura: " + obj.temp
    description = "Fase "+fase/// Temperatura: " + obj.temp
    codigoErro = parametros.codigoErroCorrenteFase;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp
    console.log(urlSet)
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoEnviaCorrenteSemaforos)
  {
    const obj = JSON.parse(message.toString());
    let jsonPost = {
      controllerItemId:obj.id,
      data:obj.leitura,
    }

    console.log(jsonPost);

    const options = {
      uri: 'https://webcentralsetrans.azurewebsites.net/api/eletric-currents/?password=webcentralsetran2021',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(jsonPost)
    }

    request.post(options,function(err,httpResponse,body){
          if (err) {
              return console.error('upload failed:', err);
          }
          console.log(httpResponse.statusCode)
      })
  }

  else if(topic == topicosMQTT.topicoDesabilitaControladora)
  {
    const obj = JSON.parse(message.toString());

    console.log(obj.id);

    arrayController.forEach(function (item, indice) 
    {
      if(item.controllerId == obj.id)
      {
        console.log(indice)
        arrayController.splice(indice,1)
        console.log(arrayController);
      }
    })
    
  }

  else if(topic == topicosMQTT.topicoControladoraSemErro)
  {
    const obj = JSON.parse(message.toString());
    let controllerItemId,description,codigoErro
    controllerItemId = obj.id
    description = ""
    //console.log(message.toString() + topic)
    codigoErro = parametros.codigoSemErro;
    let urlSet=parametros.urlGetBase
    urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
    urlSet = urlSet + "password="+parametros.passwordApi+"&"
    urlSet = urlSet + "codeError="+codigoErro+"&"
    urlSet = urlSet + "description="+description+"&"
    urlSet = urlSet + "temperature="+obj.temp    
    console.log(urlSet)
    makeGetError(urlSet)
  }

  else if(topic == topicosMQTT.topicoCallback)
  {

    let statusMessage = {
      reqId:"",
      statusCode: "",
      ciclo:"",
      data:"",
      date:"",
      latitude:"",
      longitude:"",
      version:"",
      compilation:""
    }
    const obj = JSON.parse(message.toString());

    console.log("MENSAGEM" + message.toString()+ "\n")

    if(obj.status == 100)
    {
      let tempo = obj.tp.split("/")

      let tempoFinal = moment(tempo[1], "HH:mm:ss").add(obj.gp,'seconds').format("YYYY-MM-DD/HH:mm:ss")
      statusMessage.reqId = obj.reqId,
      statusMessage.statusCode= obj.status,
      statusMessage.ciclo=obj.ci,
      statusMessage.data=obj.rd,
      statusMessage.date=tempoFinal

    }else{
      statusMessage.reqId = obj.reqId,
      statusMessage.statusCode= obj.status
      statusMessage.data=obj.leitura
      statusMessage.latitude=obj.lat
      statusMessage.longitude=obj.long
      statusMessage.version=obj.ver
      statusMessage.compilation=obj.comp
    }

   
    // arrayStatus.push(statusMessage)

    arrayStatus.push(statusMessage)
    //emitter.emit('idReq',obj.reqId)    
  }


  //client.publish(topicosMQTT.topicoCallback, "Chegou a mensagem: "+ message.toString() +
  //                                            '.\nNo Tópico: ' + topic)
  //client.end()
})

server.listen(process.env.PORT||port, function(){
    console.log(`Backend is running on port ${port}.`)

})

var checkHeartBeat = () => {
  //console.log("Checou")
  setTimeout(function() {
    // console.log(moment().format('MMMM Do YYYY, h:mm:ss a'));
    //console.log((Math.round(moment().valueOf() - arrayController[0].controllerTime)/1000))
    arrayController.forEach(function (item, indice) 
    {
      if(item)
      {
        console.log("Tempo = "+(Math.round(moment().valueOf() - item.controllerTime)/1000))
        console.log("Tamanho = " + arrayController.length)

        if((Math.round(moment().valueOf() - item.controllerTime)/1000) > 1260)
        {
          let controllerItemId,description,codigoErro
  
          console.log("Controladora sem comunicação")
          
          controllerItemId = item.controllerId
          description = "Timeout"
          codigoErro = parametros.codigoErroSemComunicação;
          let urlSet=parametros.urlGetBase
          urlSet = urlSet + "controllerItemId="+controllerItemId+"&"
          urlSet = urlSet + "password="+parametros.passwordApi+"&"
          urlSet = urlSet + "codeError="+codigoErro+"&"
          urlSet = urlSet + "description="+description
          console.log(urlSet)
          makeGetError(urlSet)
          item.controllerTime=moment()
        }
      }
    });
    
    checkHeartBeat()
  }, 60000);
}

checkHeartBeat()

module.exports = server