import 'jest'
import * as request from 'supertest'
import {URL_DEV, URL_PROD} from "./constants.js"

jest.setTimeout(70000);
const URL_TEST = URL_PROD

beforeAll(()=>{
    console.log("Start script test with automatic deploy!")
})

test( `Post in health route with id working in ${URL_TEST}`,()=>{
    return request (URL_TEST)
    .post("/health")
    .send({
        "controllerId":"3c29",
        "reqId":"3w8d"
    })
    .then(response => {
        expect(response.status).toBe(200)
        expect(response.text).toBe("OK")

    }).catch(fail)
})

test(`Post in health route with id not workin in ${URL_TEST}`,()=>{

    return request (URL_TEST)
    .post("/health")
    .send({
        "controllerId":"1111",
        "reqId":"3w8d"
    })
    
    .then(response =>{
        expect(response.status).toBe(501)
        expect(response.text).toContain("falha de comunicação com o controlador")
    })

    .catch(fail)

})

afterAll(()=>{
    console.log("Finish script test!")
})
