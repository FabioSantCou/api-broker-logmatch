parametros = {
    urlGetBase:"https://webcentralsetrans.azurewebsites.net/api/alerts/?",
    // broker:"mqtts://mensys.com.br:8883",
    broker:"mqtt://mensys.com.br:1883",
    username:"pcmqttlogmatch",
    password: '6iyvgVdh8Z',
    passwordApi:'webcentralsetran2021',
    codigoErroTensaoFase : "601",
    codigoErroPortasDigitais : "602",
    codigoErroCorrenteFase : "603",
    codigoErroGPS : "604",
    codigoErroFlashAmarelo : "605",
    codigoErroTemperatura : "606",
    codigoErroCicloMax : "607",
    codigoErroSemComunicação:"608",
    codigoErroFaseBotoeira:"609",
    codigoHeartBeat : "000",
    codigoSemErro : '001'
}

topicosMQTT = {
    
    topicoErroTensaoFase : "Mensys/Publish/Erro/Tensao/Fase",

    topicoErroPortasDigitais:"Mensys/Publish/Erro/PortasDigitais",
    
    topicoErroCorrenteFase : "Mensys/Publish/Erro/Corrente/Fase",
    
    topicoErroGPS:"Mensys/Publish/Erro/GPS",
    
    topicoErroFlashAmarelo : "Mensys/Publish/Erro/Flash/Amarelo",
    
    topicoErroTemperatura:"Mensys/Publish/Erro/Temperatura",

    topicoCallback:'Mensys/Publish/Callback',

    topicoEnviaPlanoSemaforico : 'Mensys/Publish/Plano/Semaforico',

    topicoErroCicloMax : 'Mensys/Publish/Erro/Ciclo/Max',

    topicoEnviaHeartBeat : 'Mensys/Publish/Heartbeat',

    topicoEnviaCorrenteSemaforos : 'Mensys/Publish/Corrente/Semaforos',

    topicoEnviaErroBotoeira : 'Mensys/Publish/Erro/Botoeira',

    topicoDesabilitaControladora: 'Mensys/Publish/Desabilitar/Controladora',

    topicoControladoraSemErro: "Mensys/Publish/Controladora/Sem/Erro",


    //--------------------------------------------------------------------

    topicoReferenciaTemperatura: "Mensys/Referencia/Temperatura",

    topicoReferenciaFase: "Mensys/Referencia/Fase",
    
    topicoSubscribePlanoSemaforico:"Mensys/Subscribe/Plano/Semaforico",

    topicoSubscribeChecaCorrentes: "Mensys/Subscribe/Checa/Correntes",

    topicoSubscribeComandaReleA: "Mensys/Subscribe/Comanda/ReleA",

    topicoSubscribeComandaReleB: "Mensys/Subscribe/Comanda/ReleB",

    topicoSubscribeRecebePlanoA: "Mensys/Subscribe/Recebe/PlanoA",

    topicoSubscribeRecebePlanoB: "Mensys/Subscribe/Recebe/PlanoB",
    
    topicoSubscribeRecebePlanoC: "Mensys/Subscribe/Recebe/PlanoC",

    topicoSubscribeEntradaReferencia: "Mensys/Subscribe/Entrada/Referencia",

    topicoSubscribeDesabilitaControladora: "Mensys/Publish/Desabilitar/Controladora",

    topicoSubscribeLocalizacao:"Mensys/Subscribe/Localizacao",

    topicoSubscribeFirmware:"Mensys/Subscribe/Firmware",

    topicoSubscribeTrocarId:"Mensys/Subscribe/Trocar/Id"

}

URL_DEV = "http://localhost:3001/apicontroller"
URL_PROD = "https://mensys.com.br/setrans/apicontroller/"

module.exports = {parametros,topicosMQTT,URL_DEV,URL_PROD}